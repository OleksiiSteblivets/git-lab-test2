package main

import (
	"fmt"
	"strings"
)

// ----------------------------------------------------Mumbling-------------------------------------------------------------------

func Accum(s string) string {
	result := ""

	for i := 0; i < len(s); i++ {
		result = result + strings.ToUpper(string(s[i]))

		for j := 0; j < i; j++ {
			result = result + strings.ToLower(string(s[i]))
		}
		if i != len(s)-1 {
			result = result + "-"
		}

	}

	return result
}

func main() {
	fmt.Println(Accum("abcd"))
}

//accum("abcd") -> "A-Bb-Ccc-Dddd"
//accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
//accum("cwAt") -> "C-Ww-Aaa-Tttt"
